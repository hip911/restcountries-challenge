<?php

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup as DoctrineSetup;
use RestCountriesApp\Actions\CountryAction;
use RestCountriesApp\Catalog\CountryImporter;
use RestCountriesApp\Catalog\Normalizers\CountryNormalizer;
use RestCountriesApp\Commands\ImportCountries;
use RestCountriesApp\Entities\Language;
use RestCountriesApp\Resources\CountryResource;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Serializer;

$container = $app->getContainer();

$container[EntityManager::class] = function ($c) {
    $settings = $c->get('settings')['doctrine'];
    $config = DoctrineSetup::createAnnotationMetadataConfiguration($settings['entitiesPath'], $settings['isDevMode']);
    $entityManager = EntityManager::create($settings['connection'], $config);
    return $entityManager;
};

$container[Serializer::class] = function ($c) {
    return $serializer = new Serializer(
        [new CountryNormalizer(),new ArrayDenormalizer()],
        [new JsonEncoder()]
    );
};

$container[CountryImporter::class] = function ($c) {
    $em = $c->get(EntityManager::class);
    return new CountryImporter($em);
};

$container[ImportCountries::class] = function ($c) {
    $em = $c->get(EntityManager::class);
    return new ImportCountries($c->get(CountryImporter::class),$em->getRepository(Language::class));
};

$container[RestCountriesApp\Actions\CountryAction::class] = function ($c) {
    return new CountryAction(new CountryResource($c->get(EntityManager::class),$c->get(Serializer::class)));
};

