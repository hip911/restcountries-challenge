<?php
// Routes

$app->group('/api', function () {
    $this->group('/countries', function () {
        $this->get('', RestCountriesApp\Actions\CountryAction::class.':fetch');
        $this->get('/{iso2Code}', RestCountriesApp\Actions\CountryAction::class.':fetchOne');
        $this->get('/uses-language/{language}', RestCountriesApp\Actions\CountryAction::class.':fetchByLanguage');
    });
    $this->get('/scrape-countries', RestCountriesApp\Commands\ImportCountries::class.':execute');
});
