<?php
return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],

        'doctrine' => [
            'connection' => [
                'driver'   => 'pdo_mysql',
                'user'     => '',
                'password' => '',
                'dbname'   => '',
            ],
            'entitiesPath' => [
                __DIR__ . "RestCountriesApp/Entities"
            ],
            'isDevMode' => false
        ]

    ],
];
