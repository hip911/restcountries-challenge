<?php

namespace RestCountriesApp\Commands;

use GuzzleHttp\Client;
use RestCountriesApp\Catalog\Adapter\GuzzleHttpClient;
use RestCountriesApp\Catalog\HttpCatalog;
use RestCountriesApp\Catalog\Normalizers\CountryNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;

class ImportCountries
{
    private $importer;
    private $languageRepo;

    public function __construct($importer,$languageRepo)
    {
        $this->importer = $importer;
        $this->languageRepo = $languageRepo;
    }

    public function execute()
    {
        $serializer = new Serializer([new CountryNormalizer()],[new JsonEncoder()]);
        $url = 'https://restcountries.eu/rest/v1/';
        $remoteCatalog = new HttpCatalog(new GuzzleHttpClient(new Client()),$serializer,$url);
        $this->importer->import($remoteCatalog);
    }
}