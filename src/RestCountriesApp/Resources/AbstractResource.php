<?php

namespace RestCountriesApp\Resources;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Serializer\Serializer;

abstract class AbstractResource
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    protected $serializer ;

    public function __construct(EntityManager $entityManager, Serializer $serializer)
    {
        $this->entityManager    = $entityManager;
        $this->serializer       = $serializer;
    }
}