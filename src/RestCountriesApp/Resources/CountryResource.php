<?php

namespace RestCountriesApp\Resources;

use RestCountriesApp\Entities\Country;
use RestCountriesApp\Entities\Language;

class CountryResource extends AbstractResource
{
    /**
     * @param string|null $iso2Code
     * @return array
     */
    public function get($iso2Code = null)
    {
        $criteria = $iso2Code ? ['iso2Code' => $iso2Code]:[];
        $countryRepo = $this->entityManager->getRepository(Country::class);
        $countries = $countryRepo->findBy($criteria);

        return $countries;
    }

    /**
     * @param $language
     * @return array
     */
    public function getByLanguage($language)
    {
        $languageRepo = $this->entityManager->getRepository(Language::class);
        $lang = $languageRepo->findOneBy(['language' => $language]);
        if(is_null($lang)) {
            return [];
        }
        $countryRepo = $this->entityManager->getRepository(Country::class);
        return $countryRepo->createQueryBuilder('c')
            ->innerJoin('c.languages', 'l')
            ->where('l.language = :language')
            ->setParameter('language',$lang->language)
            ->getQuery()->getResult();
    }
}