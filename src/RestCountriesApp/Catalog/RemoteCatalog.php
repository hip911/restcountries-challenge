<?php

namespace RestCountriesApp\Catalog;

interface RemoteCatalog
{
    /** @return  */
    public function fetch();

}