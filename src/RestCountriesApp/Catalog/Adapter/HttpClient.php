<?php

namespace RestCountriesApp\Catalog\Adapter;

interface HttpClient
{
    public function fetch($url);
}