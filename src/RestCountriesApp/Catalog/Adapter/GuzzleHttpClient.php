<?php

namespace RestCountriesApp\Catalog\Adapter;

use Psr\Http\Message\ResponseInterface;

class GuzzleHttpClient implements HttpClient
{
    /**
     * @var \GuzzleHttp\Client $client
     */
    private $client;

    /**
     * GuzzleHttpClient constructor.
     * @param $client
     */
    public function __construct($client)
    {
        $this->client = $client;
    }

    /**
     * @param $url
     * @return ResponseInterface
     */
    public function fetch($url)
    {
        return $this->client->get($url)->getBody()->getContents();
    }
}