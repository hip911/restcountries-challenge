<?php

namespace RestCountriesApp\Catalog;

use Doctrine\ORM\EntityManager;
use RestCountriesApp\Entities\Language;

class CountryImporter
{
    private $em;

    private $languageRepo;

    private $cachedCountries = [];

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->languageRepo = $em->getRepository(Language::class);
    }

    public function import(RemoteCatalog $remoteCatalog)
    {
        foreach ($remoteCatalog->fetch() as $key => $country) {
                $country = $this->filterAlreadyPersistedInLanguages($country);
                $country = $this->filterAlreadyPersistedInTranslations($country);
                $this->em->persist($country);
        }
        $this->em->flush();

    }

    private function filterAlreadyPersistedInLanguages($model)
    {
        foreach ($model->languages as $language) {
            if ($existingLang = $this->getExistingLanguage($language)) {
                $model->removeLanguage($language);
                $model->addLanguage($existingLang);
            } else {
                $this->cachedCountries[$language->language] = $language;
            }
        }

        return $model;
    }

    private function filterAlreadyPersistedInTranslations($model)
    {
        foreach ($model->translations as $translation) {
            if ($existingLang = $this->getExistingLanguage($translation->language)) {
                $translation->setLanguage($existingLang);
            } else {
                $this->cachedCountries[$translation->language->language] = $translation->language;
            }
        }

        return $model;
    }

    /**
     * @param $language
     * @return mixed
     */
    private function getExistingLanguage($language)
    {
            return isset($this->cachedCountries[$language->language]) ? $this->cachedCountries[$language->language]:false;
    }
}