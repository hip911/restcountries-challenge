<?php

namespace RestCountriesApp\Catalog;

use RestCountriesApp\Catalog\Adapter\HttpClient;
use RestCountriesApp\Catalog\Normalizers\CountryNormalizer;
use RestCountriesApp\Entities\Country;

class HttpCatalog implements RemoteCatalog
{
    private $client;
    private $serializer;
    private $url;

    public function __construct(HttpClient $client,$normalizer,$url)
    {
        $this->client = $client;
        $this->serializer = $normalizer;
        $this->url = $url;
    }

    public function fetch()
    {
        $data = $this->client->fetch($this->url);
        return $this->serializer->deserialize($data,Country::class,'json');
    }
}