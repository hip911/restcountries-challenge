<?php

namespace RestCountriesApp\Catalog\Normalizers;

use RestCountriesApp\Entities\Border;
use RestCountriesApp\Entities\Country;
use RestCountriesApp\Entities\Language;
use RestCountriesApp\Entities\LatLong;
use RestCountriesApp\Entities\Translation;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class CountryNormalizer implements NormalizerInterface,DenormalizerInterface
{
    /**
     * Denormalizes data back into an object of the given class.
     *
     * @param mixed $data data to restore
     * @param string $class the expected class to instantiate
     * @param string $format format the given data was extracted from
     * @param array $context options available to the denormalizer
     *
     * @return object
     */
    public function denormalize($data, $class, $format = null, array $context = array())
    {
//        var_dump($data);
//        die();
//        $data = json_decode($data,true);
        $objects = [];
        foreach ($data as $item) {
            $object = new $class;
            $object->name = $item['name'];
            $object->topLevelDomain = $item['topLevelDomain'][0];
            $object->iso2Code = $item['alpha2Code'];
            $object->iso3Code = $item['alpha3Code'];
            foreach ($item['languages'] as $lang) {
                $language = new Language();
                $language->language = $lang;
                $object->addLanguage($language);
            }

            $lat = isset($item['latlng'][0]) ? $item['latlng'][0]:0;
            $long = isset($item['latlng'][1]) ? $item['latlng'][1]:0;
            $latlong = new LatLong($lat,$long);
            $object->setLatLong($latlong);

            foreach ($item['borders'] as $code) {
                $border = new Border();
                $border->countryCode = $code;
                $object->addBorder($border);
            }

            foreach ($item['translations'] as $language => $translation) {
                $lang = new Language();
                $lang->language = $language;
                $trans = new Translation($object,$lang);
                $trans->translation = $translation;
                $object->addTranslation($trans);
            }

            $objects[] = $object;
        }

        return $objects;
    }

    /**
     * Checks whether the given class is supported for denormalization by this normalizer.
     *
     * @param mixed $data Data to denormalize from
     * @param string $type The class to which the data should be denormalized
     * @param string $format The format being deserialized from
     *
     * @return bool
     */
    public function supportsDenormalization($data, $type, $format = null)
    {
        return true;
    }

    /**
     * Normalizes an object into a set of arrays/scalars.
     *
     * @param object $object object to normalize
     * @param string $format format the normalization result will be encoded as
     * @param array $context Context options for the normalizer
     *
     * @return array|\Symfony\Component\Serializer\Normalizer\scalar
     */
    public function normalize($object, $format = null, array $context = array())
    {
        if (is_array($object)){
            return $object;
        }
        /** @var Country $object */
        return $object->jsonSerialize();
    }

    /**
     * Checks whether the given class is supported for normalization by this normalizer.
     *
     * @param mixed $data Data to normalize
     * @param string $format The format being (de-)serialized from or into
     *
     * @return bool
     */
    public function supportsNormalization($data, $format = null)
    {
        return true;
    }
}