<?php
namespace RestCountriesApp\Actions;

use RestCountriesApp\Resources\CountryResource;

final class CountryAction
{
    private $countryResource;

    public function __construct(CountryResource $countryResource)
    {
        $this->countryResource = $countryResource;
    }

    public function fetch($request, $response, $args)
    {
        $country = $this->countryResource->get();
        return $response->withJSON($country);
    }

    public function fetchByLanguage($request, $response, $args)
    {
        $country = $this->countryResource->getByLanguage($args['language']);
        return $response->withJSON($country);

    }

    public function fetchOne($request, $response, $args)
    {
        $country = $this->countryResource->get($args['iso2Code']);
        if ($country) {
            return $response->withJSON($country);
        }
        return $response->withStatus(404, 'No country found with that iso2code.');
    }
}