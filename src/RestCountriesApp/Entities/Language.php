<?php

namespace RestCountriesApp\Entities;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="languages")
 */
class Language
{
    /**
     * @Id()
     * @Column(type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @Column(type="string")
     */
    public $language;

    /**
     * @ManyToMany(targetEntity="RestCountriesApp\Entities\Country",inversedBy="languages")
     */
    public $countries;

    /**
     * @return mixed
     */
    public function getCountries()
    {
        return $this->countries;
    }

    public function __construct()
    {
        $this->countries = new ArrayCollection();
    }

    /**
     * @param mixed $country
     */
    public function addCountry($country)
    {
        $this->countries->add($country);
    }

    public function jsonSerialize()
    {
        return //[
            /*'language' => */$this->language;
        //];
    }
}