<?php

namespace RestCountriesApp\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use JsonSerializable;

/**
 * @Entity
 * @Table(name="countries")
 */
class Country implements JsonSerializable
{
    /**
     * @Id()
     * @Column(type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    public $id;
    /**
     * @Column(type="string")
     */
    public $iso2Code;
    /**
     * @Column(length=10)
     */
    public $iso3Code;
    /**
     * @Column(length=100)
     */
    public $name;
    /**
     * @Column(length=10)
     */
    public $topLevelDomain;
    /**
     * @ManyToMany(targetEntity="RestCountriesApp\Entities\Language",mappedBy="countries",cascade={"persist"})
     */
    public $languages;

    /**
     * @Embedded(class = "RestCountriesApp\Entities\LatLong")
     */
    public $latLong;

    /**
    * @ManyToMany(targetEntity="RestCountriesApp\Entities\Border", mappedBy="border",cascade={"persist"})
    */
    public $borderedBy;

    /**
     * @OneToMany(targetEntity="RestCountriesApp\Entities\Translation", mappedBy="country",cascade={"persist"})
     */
    public $translations;

    /**
     * @param mixed $latLong
     */
    public function setLatLong($latLong)
    {
        $this->latLong = $latLong;
    }

    public function __construct()
    {
        $this->languages        = new ArrayCollection();
        $this->borderedBy       = new ArrayCollection();
        $this->translations     = new ArrayCollection();
    }

    /**
     * @param mixed $translation
     */
    public function addTranslation(Translation $translation)
    {
        $this->translations->add($translation);
    }
    /**
     * @param mixed $border
     */
    public function addBorder(Border $border)
    {
        $border->addCountry($this);
        $this->borderedBy->add($border);
    }
    /**
     * @param mixed $language
     */
    public function addLanguage(Language $language)
    {
        $language->addCountry($this);
        $this->languages->add($language);
    }

    /**
     * @param Language $language
     */
    public function removeLanguage(Language $language)
    {
        $this->languages->removeElement($language);
    }

    /**
     * @return mixed
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * @return mixed
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    public function jsonSerialize()
    {
        return [
            'name' => $this->name,
            'iso2' => $this->iso2Code,
            'iso3' => $this->iso3Code,
            'latlong' => $this->latLong,
            'languages' => array_map(function ($language) {
                return $language->jsonSerialize();
            },$this->getLanguages()->getValues()),
            'translations' => array_map(function ($translation) {
                return $translation->jsonSerialize();
            },$this->getTranslations()->getValues())
        ];
    }
}