<?php

namespace RestCountriesApp\Entities;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="borders")
 */
class Border
{
    /**
     * @Id()
     * @Column(type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @Column(type="string")
     */
    public $countryCode;

    /**
     * @ManyToMany(targetEntity="RestCountriesApp\Entities\Country",inversedBy="borderedBy")
     */
    public $border;

    public function __construct()
    {
        $this->border = new ArrayCollection();
    }

    /**
     * @param mixed $country
     */
    public function addCountry($country)
    {
        $this->border->add($country);
    }
}