<?php

namespace RestCountriesApp\Entities;

/**
 * @Embeddable
 */
class LatLong
{
    /** @Column(type = "integer") */
    public $latitude;

    /** @Column(type = "integer") */
    public $longitude;

    public function __construct($latitude,$longitude)
    {
        $this->latitude     = $latitude;
        $this->longitude    = $longitude;
    }
}