<?php

namespace RestCountriesApp\Entities;

/**
 * @Entity
 * @Table(name="translations")
 */
class Translation
{
    /** @Id @ManyToOne(targetEntity="RestCountriesApp\Entities\Country") */
    public $country;

    /** @Id @ManyToOne(targetEntity="RestCountriesApp\Entities\Language",cascade={"persist"}) */
    public $language;

    /**
     * @Column(type="string",nullable=true)
     */
    public $translation;

    public function __construct(Country $country, Language $language)
    {
        $this->country  = $country;
        $this->language = $language;
    }

    /**
     * @param mixed $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @return mixed
     */
    public function getLanguage()
    {
        return $this->language;
    }

    public function jsonSerialize()
    {
        return [
              $this->getLanguage()->jsonSerialize() => $this->translation
        ];
    }
}