<?php

namespace Tests\Unit;

use Doctrine\ORM\EntityManager;
use Prophecy\Argument;
use RestCountriesApp\Catalog\CountryImporter;
use RestCountriesApp\Catalog\RemoteCatalog;
use RestCountriesApp\Entities\Country;

class ImportCountriesTest extends \PHPUnit_Framework_TestCase
{
    public function testImport()
    {
        $remoteCatalog = $this->prophesize(RemoteCatalog::class);
        $em = $this->prophesize(EntityManager::class);

        $countryOne = $this->createTestCountry();
        $countryTwo = $this->createTestCountry();

        $remoteCatalog->fetch()->willReturn([$countryOne,$countryTwo]);

        $importer = new CountryImporter($em->reveal());
        $importer->import($remoteCatalog->reveal());

        $em->persist(Argument::any())->shouldHaveBeenCalledTimes(2);
    }

    private function createTestCountry()
    {
        return new Country();
    }
}