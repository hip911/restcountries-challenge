<?php

namespace Tests\Unit;

use GuzzleHttp\Psr7\Stream;
use Psr\Http\Message\ResponseInterface;
use RestCountriesApp\Catalog\Adapter\GuzzleHttpClient;

class GuzzleHttpClientTest extends \PHPUnit_Framework_TestCase
{
    public function testGuzzleClientFetch()
    {
        $guzzle = $this->prophesize(\GuzzleHttp\Client::class);
        $response = $this->prophesize(ResponseInterface::class);
        $stream = $this->prophesize(Stream::class);
        $response->getBody()->willReturn($stream->reveal());
        $url = 'http://remote.url';
        $guzzle->get($url)->willReturn($response->reveal());

        $httpClient = new GuzzleHttpClient($guzzle->reveal());
        $httpClient->fetch($url);

        $guzzle->get($url)->shouldHaveBeenCalledTimes(1);
        $response->getBody()->shouldHaveBeenCalledTimes(1);
        $stream->getContents()->shouldHaveBeenCalledTimes(1);
    }
}