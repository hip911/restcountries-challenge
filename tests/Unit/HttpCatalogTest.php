<?php

namespace Tests\Unit;

use Prophecy\Argument;
use RestCountriesApp\Catalog\Adapter\HttpClient;
use RestCountriesApp\Catalog\HttpCatalog;
use RestCountriesApp\Catalog\Normalizers\CountryNormalizer;
use RestCountriesApp\Entities\Country;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;

class HttpCatalogTest extends \PHPUnit_Framework_TestCase
{
    public function testHttpCatalogFetch()
    {
        $sampleJson = '{"sample":"json"}';
        $url = 'http://remote.url';
        $countryOne = $this->createTestCountry();

        $client = $this->prophesize(HttpClient::class);
        $client->fetch($url)->willReturn($sampleJson);
        $jsonEncoder = $this->prophesize(JsonEncoder::class);
        $jsonEncoder->supportsDecoding('json')->willReturn(true);
        $jsonEncoder->decode($sampleJson,'json',[])->willReturn('mocked');
        $normalizer = $this->prophesize(CountryNormalizer::class);
        $normalizer->supportsDenormalization(Argument::any(),Argument::any(),Argument::any(),Argument::any())->willReturn(true);
        $normalizer->supportsNormalization(Argument::any(),Argument::any(),Argument::any(),Argument::any())->willReturn(true);
        $normalizer->denormalize(Argument::any(),Argument::any(),Argument::any(),Argument::any())->willReturn([$countryOne]);

        $serializer = new Serializer([$normalizer->reveal()],[$jsonEncoder->reveal()]);
        $catalog = new HttpCatalog($client->reveal(),$serializer,$url);
        $data = $catalog->fetch();

        $this->assertSame([$countryOne],$data);
    }

    private function createTestCountry()
    {
        return new Country();
    }
}