<?php

namespace Tests\Integration;


use RestCountriesApp\Catalog\Normalizers\CountryNormalizer;
use RestCountriesApp\Entities\Country;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;

class CountryNormalizerTest extends \PHPUnit_Framework_TestCase
{
    public function testNormalizer()
    {
        $json = file_get_contents(__DIR__.'/../Fixtures/countryFixture.json');
        $serializer = new Serializer([new CountryNormalizer()],[new JsonEncoder()]);
        $result = $serializer->deserialize($json,Country::class,'json');

        $this->assertCount(5,$result[0]->translations);
        $this->assertCount(3,$result[0]->languages);
        $this->assertCount(6,$result[0]->borderedBy);
    }
}