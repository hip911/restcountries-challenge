<?php

namespace Tests\Functional;

class CountriesRoutesTest extends BaseTestCase
{
    public function testGetCountriesWithoutIso3Code()
    {
        $response = $this->runApp('GET', '/api/countries');

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains('AFG', (string)$response->getBody());
        $this->assertNotContains('Hello', (string)$response->getBody());
    }

    public function testGetCountriesWithIso3Code()
    {
        $response = $this->runApp('GET', '/api/countries/af');

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains('Afghanistan', (string)$response->getBody());
    }

    public function testGetCountriesWhichUsesLanguage()
    {
        $response = $this->runApp('GET', '/api/countries/uses-language/ps');

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains('Afghanistan', (string)$response->getBody());
    }
}