# Restcountries Challenge

# Prerequisites
    A running MySql server
    Fill in the username, password, database name in both config/bootstrap.php (for Doctrine) and settings.php (for Slim) 
    //TODO unify configs :) 

# Installation
    composer install
    vendor/bin/doctrine orm:schema-tool:create
    php -S 0.0.0.0:8080 -t public public/index.php
    
# Running the tests
The fixtures are not linked to the DB yet, so the "scrape-countries" route needs to be executed beforehand.

    composer test    

#Starting again (to get a clean schema)

    vendor/bin/doctrine orm:schema-tool:drop --force
    
# Summary

- I have not used either symfony/serializer, doctrine/orm or slim 3 extensively before this challenge, 
and there might be hidden gems lying around ( eg public visiblity in entities and it was easier to test out functionality )
- Pimple could have been replaced with symfony-di
- $container[Serializer::class] could have been used as a factory with 'CountryNormalizer::class' parameter

- Tried to aim for denormalized schema
- I did spend more than two hours, mainly because I wanted to try out these new tools, even retyping from zero could be barely done in two hours 
- Ran into a few interesting problems regarding Doctrine (to cache the Language belonging to a Country for relinkablity)
- persist() in a loop with a database lookup for duplication was about 10x performance drop